FROM golang:1.12.6-alpine3.10 AS build

RUN apk add --no-cache git
RUN go get gitlab.com/shasderias/hcloud-update-floating-ip

FROM alpine:latest

RUN apk add --no-cache keepalived

ADD scripts/keepalived-notify.sh /usr/local/bin
RUN chmod 0700 /usr/local/bin/keepalived-notify.sh

COPY --from=build /go/bin/hcloud-update-floating-ip /usr/local/bin/hcloud-update-floating-ip

RUN mkdir -p /etc/keepalived

CMD ["keepalived", "-nl"]
