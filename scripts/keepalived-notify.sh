#!/bin/sh

TYPE=$1
NAME=$2
STATE=$3

if [[ $STATE == "MASTER" ]]; then
  hcloud-update-floating-ip $(hostname -f) $FLOATING_IP
fi
